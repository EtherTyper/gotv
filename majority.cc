// Copyright 2020 Eli Bradley
#include "./voting.h"

namespace vote {

std::optional<std::string> majority(const std::vector<SingleBallot> ballots) {
  std::optional<SingleBallot> max;
  double sum = 0;

  for (auto ballot : ballots) {
    sum += ballot.weight;

    if (!max.has_value() || max->weight < ballot.weight) {
      max = ballot;
    }
  }

  if (max.has_value() && max->weight > sum / 2) {
    return max->candidate;
  } else {
    return std::optional<std::string>{};
  }
}

}  // namespace vote
