#!/bin/sh
gfind . -regex '.*\.\(cc\|h\)' -exec echo {} \; -exec clang-format -style=file -i {} \;
