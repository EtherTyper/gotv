// Copyright 2020 Eli Bradley
#include <unordered_map>
#include <unordered_set>

#include "./voting.h"

namespace vote {

class BallotReference {
 public:
  RCVBallot& reference;
  BallotReference(RCVBallot& reference) : reference(reference) {}  // NOLINT
  RCVBallot& operator*() const { return reference; }
  operator RCVBallot&() const { return reference; }
};

std::vector<std::string> stv(uint32_t seats,
                             const std::vector<RCVBallot> ballots) {
  std::vector<RCVBallot> modifiableBallots = ballots;
  std::unordered_set<std::string> eliminated;
  std::vector<std::string> electees;

  while (electees.size() < seats) {
    std::unordered_map<std::string, double> voteTotals;
    std::unordered_map<std::string, std::vector<BallotReference>> ballotsFor;
    double sum = 0;

    for (RCVBallot& ballot : modifiableBallots) {
      for (auto candidate : ballot.rankedCandidates) {
        if (!eliminated.contains(candidate)) {
          voteTotals[candidate] += ballot.weight;
          ballotsFor[candidate].push_back(ballot);
          sum += ballot.weight;
          break;
        }
      }
    }

    if (voteTotals.empty()) {
      return electees;
    }

    std::optional<std::pair<std::string, double>> max;
    std::optional<std::pair<std::string, double>> min;

    for (auto candidate : voteTotals) {
      if (!max.has_value() || max->second < candidate.second) {
        max = candidate;
      }
      if (!min.has_value() || min->second > candidate.second) {
        min = candidate;
      }
    }

    if (max->second > sum / seats) {
      electees.push_back(max->first);
      eliminated.insert(max->first);

      auto factor = (max->second - sum / seats) / max->second;
      for (auto ballot : ballotsFor[max->first]) {
        (*ballot).weight *= factor;
      }
    } else {
      eliminated.insert(min->first);
    }
  }

  return electees;
}

}  // namespace vote
