// Copyright 2020 Eli Bradley
#include <iostream>
#include <string>

#include "./apportionment.h"
#include "./voting.h"

int main(int argc, char **argv) {
  std::vector<vote::SingleBallot> texas_senate_2020 = {
      {"John Cornyn", 5960155},
      {"MJ Hegar", 4888110},
      {"Kerry McKennon", 209586},
      {"David Collins", 81773}};

  std::vector<vote::SingleBallot> michigan_senate_2020 = {
      {"Gary Peters", 2737431},
      {"John James", 2643552},
      {"Valerie Willis", 50640},
      {"Marcia Squier", 39251},
      {"Doug Dern", 13101}};

  std::vector<vote::RCVBallot> rcv_example = {
      {{"Republican", "Libertarian", "Democrat"}, 35},
      {{"Libertarian", "Republican", "Democrat"}, 25},
      {{"Democrat", "Libertarian", "Republican"}, 35},
      {{"Democrat", "Republican"}, 5}};

  std::vector<vote::RCVBallot> at_large_example = {
      {{"R1", "R2", "L1", "L2", "D1", "D2"}, 45},
      {{"L1", "L2", "R1", "R2", "D1", "D2"}, 30},
      {{"D1", "D2", "L1", "L2", "R1", "R2"}, 25}};

  std::vector<vote::RCVBallot> schulze_example = {
      {{"A", "C", "B", "E", "D"}, 5}, {{"A", "D", "E", "C", "B"}, 5},
      {{"B", "E", "D", "A", "C"}, 8}, {{"C", "A", "B", "E", "D"}, 3},
      {{"C", "A", "E", "B", "D"}, 7}, {{"C", "B", "A", "D", "E"}, 2},
      {{"D", "C", "E", "B", "A"}, 7}, {{"E", "B", "A", "D", "C"}, 8}};

  std::vector<apportionment::State> apportionment_example = {
      {"California", 39.51}, {"Texas", 29}, {"Utah", 3.206}};

  std::cout << "FPTP Texas Senate: " << *vote::fptp(texas_senate_2020)
            << std::endl;
  std::cout << "Majority Texas Senate: " << *vote::majority(texas_senate_2020)
            << std::endl;
  std::cout << "FPTP Michigan Senate: " << *vote::fptp(michigan_senate_2020)
            << std::endl;
  std::cout << "Majority Michigan Senate: "
            << vote::majority(michigan_senate_2020).has_value() << std::endl;

  auto irv_winner = *vote::irv(rcv_example);
  std::cout << "IRV winner: " << irv_winner << std::endl;
  std::cout << "Is IRV winner a Condorcet winner: "
            << vote::isCondorcet(irv_winner, rcv_example) << std::endl;
  std::cout << "Is Republican a Condorcet winner: "
            << vote::isCondorcet("Republican", rcv_example) << std::endl;
  std::cout << "Is Libertarian a Condorcet winner: "
            << vote::isCondorcet("Libertarian", rcv_example) << std::endl;
  std::cout << "Is Democrat a Condorcet winner: "
            << vote::isCondorcet("Democrat", rcv_example) << std::endl;

  auto stv_winners = vote::stv(4, at_large_example);
  for (auto candidate : stv_winners) {
    std::cout << "STV winner: " << candidate << std::endl;
  }

  auto schulze_order = vote::schulze(schulze_example);
  for (auto candidate : schulze_order) {
    std::cout << "Schulze order: " << candidate << std::endl;
  }

  auto total_population =
      apportionment::total_population(apportionment_example);
  auto utah_quota = apportionment::quota(20, total_population, {"Utah", 3.206});
  std::cout << "Total Population: " << total_population << std::endl;
  std::cout << "Utah quota: " << utah_quota << std::endl;

  auto lf_apportionment =
      apportionment::largest_fractions(20, apportionment_example);

  for (auto state : lf_apportionment) {
    std::cout << "LF - Allocated to " << state.first.name << ": "
              << state.second << std::endl;
  }

  auto gd_apportionment =
      apportionment::greatest_divisors(20, apportionment_example);

  for (auto state : gd_apportionment) {
    std::cout << "GD - Allocated to " << state.first.name << ": "
              << state.second << std::endl;
  }

  auto mf_apportionment =
      apportionment::major_fractions(20, apportionment_example);

  for (auto state : mf_apportionment) {
    std::cout << "MF - Allocated to " << state.first.name << ": "
              << state.second << std::endl;
  }

  auto sd_apportionment =
      apportionment::smallest_divisors(20, apportionment_example);

  for (auto state : sd_apportionment) {
    std::cout << "SD - Allocated to " << state.first.name << ": "
              << state.second << std::endl;
  }

  auto ep_apportionment =
      apportionment::equal_proportions(20, apportionment_example);

  for (auto state : ep_apportionment) {
    std::cout << "EP - Allocated to " << state.first.name << ": "
              << state.second << std::endl;
  }

  return 0;
}
