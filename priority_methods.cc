// Copyright 2020 Eli Bradley
#include <cmath>

#include "./apportionment.h"

namespace apportionment {

std::map<State, uint32_t> equal_proportions(uint32_t total_seats,
                                            const std::vector<State> states) {
  return priority_method(total_seats, states,
                         [](uint32_t seats, double population) {
                           return population / sqrt(seats * (seats + 1));
                         });
}

}  // namespace apportionment
