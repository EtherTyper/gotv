// Copyright 2020 Eli Bradley
#ifndef APPORTIONMENT_H_
#define APPORTIONMENT_H_

#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace apportionment {

class State {
 public:
  std::string name;
  double weight;

  inline bool operator<(const State& other) const { return name < other.name; }
};

double quota(uint32_t total_seats, double total_population, const State state);
double total_population(const std::vector<State> population);

// a must be a function of quota and lambda which decreases with lambda.
// It represents the number of seats a state should be allotted.
template <typename T>
inline std::map<State, uint32_t> lambda_method(uint32_t total_seats,
                                               const std::vector<State> states,
                                               T a) {
  double total_pop = total_population(states);
  auto total_seats_at = [&](double lambda) {
    uint32_t seats_assigned = 0;
    for (auto state : states) {
      seats_assigned += a(quota(total_seats, total_pop, state), lambda);
    }
    return seats_assigned;
  };

  // Binary search for appropriate lambda.
  double lower_bound = 1;
  double upper_bound = 1;
  while (total_seats_at(lower_bound) < total_seats) lower_bound /= 2;
  while (total_seats_at(upper_bound) >= total_seats) upper_bound *= 2;
  while (total_seats_at(lower_bound) != total_seats) {
    double middle = (lower_bound + upper_bound) / 2;
    if (total_seats_at(middle) >= total_seats) {
      lower_bound = middle;
    } else {
      upper_bound = middle;
    }
  }
  auto found_lambda = lower_bound;

  // Find apportionment.
  std::map<State, uint32_t> seats;
  for (auto state : states) {
    seats[state] = a(quota(total_seats, total_pop, state), found_lambda);
  }
  return seats;
}

// a must be a function of state seats and state population.
// It represents the priority this state should receive another seat.
template <typename T>
inline std::map<State, uint32_t> priority_method(
    uint32_t total_seats, const std::vector<State> states, T a) {
  std::map<State, uint32_t> seats;
  std::multimap<double, State> priority;

  uint32_t seats_assigned = states.size();
  for (auto state : states) {
    seats[state] = 1;
    priority.insert({a(seats[state], state.weight), state});
  }

  while (seats_assigned < total_seats && !priority.empty()) {
    auto iterator = --priority.end();
    auto state = (*iterator).second;
    priority.erase(iterator);

    seats[state]++;
    seats_assigned++;

    priority.insert({a(seats[state], state.weight), state});
  }

  return seats;
}

std::map<State, uint32_t> largest_fractions(uint32_t total_seats,
                                            const std::vector<State> states);
std::map<State, uint32_t> greatest_divisors(uint32_t total_seats,
                                            const std::vector<State> states);
std::map<State, uint32_t> major_fractions(uint32_t total_seats,
                                          const std::vector<State> states);
std::map<State, uint32_t> equal_proportions(uint32_t total_seats,
                                            const std::vector<State> states);
std::map<State, uint32_t> smallest_divisors(uint32_t total_seats,
                                            const std::vector<State> states);

}  // namespace apportionment

#endif  // APPORTIONMENT_H_
