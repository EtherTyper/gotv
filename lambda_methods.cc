// Copyright 2020 Eli Bradley
#include <cmath>

#include "./apportionment.h"

namespace apportionment {

std::map<State, uint32_t> greatest_divisors(uint32_t total_seats,
                                            const std::vector<State> states) {
  return lambda_method(total_seats, states, [](double quota, double lambda) {
    return floor(quota / lambda);
  });
}
std::map<State, uint32_t> major_fractions(uint32_t total_seats,
                                          const std::vector<State> states) {
  return lambda_method(total_seats, states, [](double quota, double lambda) {
    return floor(quota / lambda + 0.5);
  });
}
std::map<State, uint32_t> smallest_divisors(uint32_t total_seats,
                                            const std::vector<State> states) {
  return lambda_method(total_seats, states, [](double quota, double lambda) {
    return ceil(quota / lambda);
  });
}

}  // namespace apportionment
