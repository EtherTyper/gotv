// Copyright 2020 Eli Bradley
#ifndef VOTING_H_
#define VOTING_H_

#include <cstdint>
#include <optional>
#include <string>
#include <vector>

namespace vote {

class SingleBallot {
 public:
  std::string candidate;
  double weight;

  inline bool operator=(const SingleBallot& other) const {
    return candidate == other.candidate && weight == other.weight;
  }
};

class RCVBallot {
 public:
  std::vector<std::string> rankedCandidates;
  double weight;

  inline bool operator=(const RCVBallot& other) const {
    return rankedCandidates == other.rankedCandidates && weight == other.weight;
  }
};

std::optional<std::string> fptp(const std::vector<SingleBallot> ballots);
std::optional<std::string> majority(const std::vector<SingleBallot> ballots);
std::optional<std::string> irv(const std::vector<RCVBallot> ballots);

std::vector<std::string> stv(uint32_t seats,
                             const std::vector<RCVBallot> ballots);

// All candidates must be listed on all ballots.
std::vector<std::string> schulze(const std::vector<RCVBallot> ballots);

const bool isCondorcet(const std::string winner,
                       const std::vector<RCVBallot> ballots);

}  // namespace vote

#endif  // VOTING_H_
