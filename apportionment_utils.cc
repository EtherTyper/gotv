// Copyright 2020 Eli Bradley
#include "./apportionment.h"

namespace apportionment {

double quota(uint32_t total_seats, double total_population, const State state) {
  return state.weight / total_population * total_seats;
}

double total_population(const std::vector<State> population) {
  double sum = 0;
  for (auto state : population) {
    sum += state.weight;
  }
  return sum;
}

}  // namespace apportionment
