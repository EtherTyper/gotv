// Copyright 2020 Eli Bradley
#include <cmath>
#include <map>

#include "./apportionment.h"

namespace apportionment {

std::map<State, uint32_t> largest_fractions(uint32_t total_seats,
                                            const std::vector<State> states) {
  std::map<State, uint32_t> seats;
  std::multimap<double, State> fractional;
  uint32_t seats_assigned = 0;
  double total_pop = total_population(states);

  // Get quota for each state, and assign each state floor(quota) seats.
  for (auto state : states) {
    double quot = quota(total_seats, total_pop, state);
    seats_assigned += (uint32_t)quot;
    seats[state] = (uint32_t)quot;
    fractional.insert({quot - (uint32_t)quot, state});
  }

  // Give remaining seats to states of quotas with the largest fractional parts.
  for (auto iterator = fractional.end();
       seats_assigned != total_seats && iterator != fractional.begin();) {
    seats[(--iterator)->second]++;
    seats_assigned++;
  }

  return seats;
}

}  // namespace apportionment
