OFILES = $(addprefix build/, $(addsuffix .o, $(basename $(wildcard *.cc))))
DFILES = $(OFILES:%.o=%.d)
all : $(DFILES) build/tests

build/%.d: %.cc
	mkdir -p build
	g++ -M $< > $@

build/%.o : %.cc
	mkdir -p build
	g++ -MMD -MP -c -std=c++2a -o $@ $<

build/elections.a : $(filter-out build/tests.o, $(OFILES))
	mkdir -p build
	ar crs build/elections.a $^

build/tests : build/tests.o build/elections.a
	mkdir -p build
	g++ -std=c++2a -o build/tests build/tests.o build/elections.a

clean:
	rm -rf build

include $(DFILES)
