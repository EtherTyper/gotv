// Copyright 2020 Eli Bradley
#include <unordered_map>

#include "./voting.h"

namespace vote {

const bool isCondorcet(const std::string winner,
                       const std::vector<RCVBallot> ballots) {
  std::unordered_map<std::string, double> winnerAdvantage;

  for (auto ballot : ballots) {
    bool foundWinner = false;
    for (auto candidate : ballot.rankedCandidates) {
      if (candidate == winner) {
        foundWinner = true;
        continue;
      }

      if (foundWinner) {
        winnerAdvantage[candidate] += ballot.weight;
      } else {
        winnerAdvantage[candidate] -= ballot.weight;
      }
    }
  }

  for (auto entry : winnerAdvantage) {
    if (entry.second < 0) {
      return false;
    }
  }

  return true;
}

}  // namespace vote
