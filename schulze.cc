// Copyright 2020 Eli Bradley
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <utility>

#include "./voting.h"

namespace vote {

std::vector<std::string> schulze(const std::vector<RCVBallot> ballots) {
  std::unordered_map<std::string, std::unordered_map<std::string, double>>
      preference;

  for (auto ballot : ballots) {
    for (auto candidateA : ballot.rankedCandidates) {
      bool foundA = false;

      for (auto candidateB : ballot.rankedCandidates) {
        if (candidateA == candidateB) {
          foundA = true;
          continue;
        }

        if (foundA) {
          preference[candidateA][candidateB] += ballot.weight;
        }
      }
    }
  }

  std::vector<std::string> candidates;
  for (auto entry : preference) {
    candidates.push_back(entry.first);
  }

  // Floyd-Warshall to find maximum path strengths.
  auto strength = move(preference);
  for (auto candidateB : candidates) {
    for (auto candidateA : candidates) {
      for (auto candidateC : candidates) {
        auto new_path_strength = std::min(strength[candidateA][candidateB],
                                          strength[candidateB][candidateC]);
        strength[candidateA][candidateC] =
            std::max(strength[candidateA][candidateC], new_path_strength);
      }
    }
  }

  std::sort(candidates.begin(), candidates.end(),
            [&](const std::string& candidateA, const std::string& candidateB) {
              return strength[candidateA][candidateB] >
                     strength[candidateB][candidateA];
            });

  return candidates;
}

}  // namespace vote
