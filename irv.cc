// Copyright 2020 Eli Bradley
#include <unordered_map>
#include <unordered_set>

#include "./voting.h"

namespace vote {

std::optional<std::string> irv(const std::vector<RCVBallot> ballots) {
  std::unordered_set<std::string> eliminated;
  while (true) {
    std::unordered_map<std::string, double> voteTotals;
    double sum = 0;

    for (auto ballot : ballots) {
      for (auto candidate : ballot.rankedCandidates) {
        if (!eliminated.contains(candidate)) {
          voteTotals[candidate] += ballot.weight;
          sum += ballot.weight;
          break;
        }
      }
    }

    if (voteTotals.empty()) {
      return std::optional<std::string>{};
    }

    std::optional<std::pair<std::string, double>> max;
    std::optional<std::pair<std::string, double>> min;

    for (auto candidate : voteTotals) {
      if (!max.has_value() || max->second < candidate.second) {
        max = candidate;
      }
      if (!min.has_value() || min->second > candidate.second) {
        min = candidate;
      }
    }

    if (max->second >= sum / 2) {
      return max->first;
    } else {
      eliminated.insert(min->first);
    }
  }
}

}  // namespace vote
