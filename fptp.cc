// Copyright 2020 Eli Bradley
#include "./voting.h"

namespace vote {

std::optional<std::string> fptp(const std::vector<SingleBallot> ballots) {
  std::optional<SingleBallot> max;

  for (auto ballot : ballots) {
    if (!max.has_value() || max->weight < ballot.weight) {
      max = ballot;
    }
  }

  if (max.has_value()) {
    return max->candidate;
  } else {
    return std::optional<std::string>{};
  }
}

}  // namespace vote
